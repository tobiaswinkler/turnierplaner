app.factory('authenticationService', ['$http', '$rootScope', '$timeout', 'userService', function ($http, $rootScope, $timeout, userService) {

   var service = {};
 
        service.Login = Login;
        service.SetCredentials = SetCredentials;
        service.ClearCredentials = ClearCredentials;
        service.GetTournaments = GetTournaments;
        service.GetTournamentById = GetTournamentById;
        service.DeleteTournament = DeleteTournament;
        service.GetTournamentsAsEditor = GetTournamentsAsEditor;
        service.CheckUserByEmail = CheckUserByEmail;
        service.AddEditor = AddEditor;
        
        return service;
 
        function Login(userLogin, callback) {
            $http.post('/TurnierPlanerWebApp/rest/user/login', JSON.stringify(userLogin),{headers:{'Content-Type': 'application/json'}})
                    .then(function(response) {
                        callback(response);
                    });
        }
        
        function DeleteTournament(id, callback){
            $http.delete('rest/tournaments/deleteTournament/'+id+'?')
             .then(function(response){
                 callback(response);
              });
        }
        
        function GetTournamentsAsEditor(callback){
            $http.get('rest/tournaments/geteditabletournament/'+$rootScope.globals.email+'?')
             .then(function(response){
                 callback(response);
              });
          
        }
 
        function GetTournamentById(id, callback){
            $http.get('rest/tournaments/gettournamentbyId/'+id+'?')
             .then(function(response){
                 callback(response);
              });
        }
       
        function GetTournaments(callback){
            $http.get('rest/tournaments/gettournaments/'+$rootScope.globals.email+'?')
             .then(function(response){
                 callback(response);
              });
          
        }
 
        function CheckUserByEmail(email, callback){
            $http.get('rest/user/checkuserbyemail/'+email+'?')
             .then(function(response){
                 callback(response);
              });
        }
 
        function AddEditor(email, index, callback){
            $http.post('rest/tournaments/addEditor/'+email+'/'+index)
             .then(function(response){
                 callback(response);
              });
        }
 
        function SetCredentials(userData) {
                localStorage.setItem("currentUser",JSON.stringify(userData));
        }
 
        function ClearCredentials() {
            $rootScope.globals = {};
            localStorage.clear();
        }

    }]);


