//Controller zuständig für die einzelnen Turniere
app.controller('CreateTournamentController', ['$scope', '$rootScope', '$location', '$routeParams', '$http', 'FlashService', 'authenticationService',
    function ($scope, $rootScope, $location, $routeParams, $http, FlashService, authenticationService) {

        angular.element(document).ready(function () {
            if ($routeParams.tournamentIndex > 0) {
                $scope.isEditMode = true;
            } else {
                $scope.isEditMode = false;
            }
            $scope.init();
        });
        
        //Funktion zum speichern neuer Turniere
        $scope.saveNewTournament = function () {
            $scope.fields.email = $rootScope.globals.email;

            $http.post('rest/tournaments/createtournament/', JSON.stringify($scope.fields))
                    .then(function (response) {
                        if (response.status === 200) {
                            FlashService.Success(response.data, true);
                            $location.path('/myTournaments_view');
                        } else {
                            FlashService.Error(response.data, true);
                            $location.path('/createTournament_view');
                        }
                    });
        };

        $scope.init = function () {
            if ($scope.isEditMode) {

                authenticationService.GetTournamentById($routeParams.tournamentIndex, function (response) {
                    if (response.status === 200) {
                        $scope.fields = response.data;
                        console.log(response.data);
                        var teamsCount = response.data.teams.length;
                        $scope.fields.count = teamsCount;
                        $scope.changedTeamCount();

                    }
                });

                $scope.newTournamentHeader = "Turnier bearbeiten";
                $scope.submitBtnTxt = "Speichern";
            } else {
                $scope.newTournamentHeader = "Neues Turnier erstellen";
                $scope.submitBtnTxt = "Turnier anlegen";
                $scope.fields = {
                    id: -1, name: "", tournamentId: null, count: 3, match_duration: 15, tournament_type_type: "Round Robin", state_type_type: "ausstehend"
                };

                $scope.fields.teams = [{id: 1, name: "Team 1"}, {id: 2, name: "Team 2"}, {id: 3, name: "Team 3"}];
                $scope.changedTeamCount();
            }
            $scope.$apply();
        };

        $scope.changeTournamentRoundRobinResults = function () {
            $scope.fields.resultsRoundRobin = {groups: []};
            for (var i = 1; i <= $scope.fields.count; i++) {
                for (var x = i; x <= $scope.fields.count; x++) {
                    if (i !== x) {
                        $scope.fields.resultsRoundRobin.groups.push({home: i, guest: x, scoreHome: 0, scoreGuest: 0, result: "even"});
                    }
                }
            }
        };

        $scope.changeTournamentMixResults = function () {
            var teamscount = $scope.fields.count;
            var groupscount;
            if (teamscount % 2 !== 0) {
                teamscount++;
            }
            groupscount = Math.round(Math.log2(teamscount));

            var teamNameCounter = 0;
            $scope.fields.resultsMix = {groups: []};
            for (var i = 1; i <= groupscount; i++) {
                $scope.fields.resultsMix.groups.push({id: i, name: "Gruppe " + String.fromCharCode(64 + i), teams: []});
                for (var x = 1; x <= (teamscount / groupscount); x++) {
                    if ($scope.fields.teams[teamNameCounter] !== null) {
                        $scope.fields.resultsMix.groups[i - 1].teams.push({
                            phase: 1, results: [{
                                    id: x, name: $scope.fields.teams[teamNameCounter].name, results: {
                                        games: 0, loose: 0, even: 0, win: 0, score: 0, concede: 0, diff: 0, points: 0}}]});
                        teamNameCounter++;
                    }
                }
            }
        };

        $scope.changeTournamentKOResults = function () {
            var teamscount = $scope.fields.count;
            var groupscount;
            if (teamscount % 2 !== 0) {
                teamscount++;
            }
            groupscount = Math.round(Math.log2(teamscount));

            var teamNameCounter = 0;
            $scope.fields.resultsKO = {groups: []};
            for (var i = 1; i <= groupscount; i++) {
                $scope.fields.resultsKO.groups.push({id: i, name: "Gruppe " + i, phase: 1, teams: []});
                for (var x = 1; x <= (teamscount / groupscount); x++) {
                    if ($scope.fields.teams[teamNameCounter] !== null) {
                        $scope.fields.resultsKO.groups[i - 1].teams.push({
                            id: x, name: $scope.fields.teams[teamNameCounter].name, win: 0, loose: 0});
                        teamNameCounter++;
                    }
                }
            }
        };

        $scope.changedTeamCount = function () {
            var length = $scope.fields.count;
            if ($scope.fields.tournament_type_type !== null) {
                if ($scope.fields.tournament_type_type === "Round Robin") {
                    if (length < 3) {
                        return;
                    }
                } else {
                    if (length < 4) {
                        return;
                    }
                }
            }

            for (i = $scope.fields.teams.length; i < $scope.fields.count; i++) {
                $scope.fields.teams.push({id: (i + 1), name: "Team " + (i + 1)});
            }

            for (i = $scope.fields.teams.length; i >= $scope.fields.count; i--) {
                $scope.fields.teams.splice(i, 1);
            }
        };

        $scope.typeChanged = function () {
            if ($scope.fields.tournament_type_type !== "Round Robin") {
                if ($scope.fields.count < 4) {
                    $scope.fields.count = 4;
                    $scope.changedTeamCount();
                }
                $("#count").attr("placeholder", "mind. 4 Teams");
            } else {
                $("#count").attr("placeholder", "mind. 3 Teams");
            }

        }
    }]);
