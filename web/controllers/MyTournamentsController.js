
app.controller('MyTournamentsController', ['$scope', '$rootScope', '$location', '$http', 'FlashService', 'authenticationService',
    function ($scope, $rootScope, $location, $http, FlashService, authenticationService) {
        $scope.tournaments = [];
        authenticationService.GetTournaments(function (response) {
            if (response.status === 200) {
                var trnmts = response.data.tournaments;
                if (trnmts != null) {
                    $scope.tournaments = trnmts;
                    console.log($scope.tournaments);

                }
            }
        });

        $scope.editabletournaments = [];
        authenticationService.GetTournamentsAsEditor(function (response) {
            if (response.status === 200) {
                var trnmts = response.data.tournaments;
                if (trnmts != null) {
                    $scope.editabletournaments = trnmts;
                }
            }
        });

        $scope.showTournament = function (index) {
            $location.path('/roundRobin_view/' + index);
        };

        $scope.editTournament = function (index) {
            $location.path('/createTournament_view/' + index);
        };
        $scope.deleteTournament = function (index) {
            authenticationService.DeleteTournament(index, function (response) {
                if (response.status === 200) {
                    for (var i = 0; i < $scope.tournaments; i++) {
                        if ($scope.tournaments.tournamentId === index) {
                            var index = $scope.tournaments.indexOf(i);
                            $scope.tournaments.splice(index, 1);
                        }
                    }
                    FlashService.Success(response.data, true);

                } else {
                    FlashService.Error(response.data, true);
                }
            });
        };

        $scope.addEditor = function (index) {
            var useremail = document.getElementById("editorname").value;
            if (useremail.length > 0) {
                authenticationService.CheckUserByEmail(useremail, function (response) {
                    if (response.status === 200) {
                        authenticationService.AddEditor(useremail, index, function (response) {
                            if (response.status === 200) {
                                FlashService.Success("Editor wurde erfolgreich hinzugefuegt");
                            } else {
                                FlashService.Error("Editor konnte nicht hinzugefuegt werden");
                            }
                        });
                    } else {
                        FlashService.Error("Editor konnte nicht hinzugefuegt werden");
                    }
                    document.getElementById("editorname").setAttribute("value", "");
                });
            }
            else{
                     FlashService.Error("Editor konnte nicht hinzugefuegt werden");
            }
        }

    }]);
