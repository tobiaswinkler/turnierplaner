app.controller("RegisterViewController", [ 'userService', '$location','FlashService',
    function(userService, $location,FlashService) {
        
        var vm = this;
 
        vm.register = register;
        //Register Funktion
        function register() {
            //Ruft vom userService die Create Funktion auf und übergibt das vm.user Objekt
               userService.Create(vm.user)
               //Bekommt ein Responseobjekt zurück
                .then(
                function (response) {
                   //Erfolgreiche Anmeldung
                    if (response.status === 200) {
                        FlashService.Success(response.data + "!", true);
                        $location.path('/login_view');
                    } else {
                        //Nutzer schon vorhanden
                        if(response.status === 204){
                           FlashService.Error("E-Mail Adresse bereits Vorhanden!");
                        }
                        else{
                           FlashService.Error("Registrierung fehlgeschlagen!");
                        }
                    }
                });
        }
    
   
   
    }
]);