app.controller('LoginViewController',['$scope','authenticationService','FlashService','$location','$rootScope','$timeout',function($scope,authenticationService, FlashService, $location,$rootScope,$timeout) {
       
        
        var vm = this;
 
        vm.login = login;
        
        //Login Funktion
        function login() {
           //Aufruf der Login-Funktion im Authentication Service, übergibt das vm Objekt und bekommt ein Callback zurück.
                //Reaktion auf den Callback.
            authenticationService.Login(vm, function (response) {  

                if (response.status === 200) {
                    $rootScope.globals = response.data;
                    authenticationService.SetCredentials(response.data);
                    FlashService.Success('Login erfolgreich', true);
                    $location.path('/welcome_view');
                    //Nutzer nicht registriert
                } else if(response.status === 203) {
                    FlashService.Error('Benutzerkonto wurde noch nicht aktiviert!');
                    //Falsche eingabe durch den Nutzer
                } else {
                    FlashService.Error("E-Mail-Adresse oder Passwort falsch", true);
                }
            });
        };
}]);

