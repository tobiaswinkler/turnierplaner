//Das Haupmodul der Applikation, ngRoute wird injiziert.
var app = angular.module('turnierPlanerApp', ['ngRoute']);
//Konfigurationsphase
app.config(['$routeProvider', function($routeProvider){
//Die einzelnen Seiten Routes werden definiert und die Controller werden zugewiesen.
    $routeProvider
        .when('/register_view', {

        templateUrl: 'views/register_view.html',
        controller: 'RegisterViewController',
        //Controller kann nun überall über "vm" angesprochen werden.
        controllerAs: "vm"
        })
        .when('/createTournament_view/:tournamentIndex', {
            templateUrl: 'views/createTournament_view.html',
            controller: 'CreateTournamentController'

        })
        .when('/welcome_view', {
            templateUrl: 'views/welcome_view.html',
            controller: 'WelcomeViewController',
            controllerAs: "vm"
        })
        .when('/createTournament_view', {
            templateUrl: 'views/createTournament_view.html',
            controller: 'CreateTournamentController'
        })
        .when('/login_view', {
            templateUrl: 'views/login_view.html',
            controller: 'LoginViewController',
            controllerAs: "vm"
        })
        .when('/myTournaments_view', {
            templateUrl: 'views/myTournaments_view.html',
            controller: 'MyTournamentsController'
        })
        .when('/roundRobin_view/:tournamentId', {
            templateUrl: 'views/roundRobin_view.html',
            controller: 'RoundRobinController'
        })
        .when('/home_view', {
            templateUrl: 'views/home_view.html',
            controller: 'HomeViewController',
            controllerAs: "vm"

        })

        .when('/submitForm/:alertTitle/:alertMessage/:redirectAddress', {
        templateUrl: 'views/submitForm.html',
        controller: 'SubmitController'
        })
        .otherwise({
            redirectTo: '/home_view'
        });
}]);


//Wird nach der Konfiguration und vor Aufruf der Controller aufgerufen.
app.run(['$rootScope', '$location', '$http', function($rootScope, $location, $http) {
        
        // Nach Seitenreload bleibt der Benutzer eingeloggt.
        $rootScope.globals = JSON.parse(localStorage.getItem("currentUser")) || {};
        
        //Verhindert das sich ein nicht angemeldeter Benutzer einloggen kann
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            var restrictedPage = $.inArray($location.path(), ['/login_view', '/register_view','/home_view']) === -1;
            var loggedIn = $rootScope.globals.registered;
            if (restrictedPage && !loggedIn) {
                $location.path('/login_view');
            }
        });

}]);

//Setzt den momentanen Tab immer auf aktiv
app.controller('SetTabActiveController', function($scope, $location) {
    $scope.isActive = function(route) {
        return route === $location.path();
    };
});
//Controller für die Index.html
app.controller('MainController', ['$scope','FlashService','authenticationService','$location', function($scope,FlashService,authenticationService,$location) {
    $scope.appTitle = "Turnier Planer";
    $scope.logout = function(){
       authenticationService.ClearCredentials();
       FlashService.Success('Erfolgreich ausgeloggt.', true);
    };

 
   
}]);






