package Model;
public class Match {
    
    private int id;
    private int tournament_id;
    private int scoreH;
    private int scoreG;
    private int teamH_id;
    private int teamG_id;

    public Match() {
    }

    public Match(int id, int tournament_id, int scoreH, int scoreG, int teamH_id, int teamG_id) {
        this.id = id;
        this.tournament_id = tournament_id;
        this.scoreH = scoreH;
        this.scoreG = scoreG;
        this.teamH_id = teamH_id;
        this.teamG_id = teamG_id;
    }

    public Match(int tournament_id, int teamH_id, int teamG_id) {
        this.tournament_id = tournament_id;
        this.teamH_id = teamH_id;
        this.teamG_id = teamG_id;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTournament_id() {
        return tournament_id;
    }

    public void setTournament_id(int tournament_id) {
        this.tournament_id = tournament_id;
    }

    public int getScoreH() {
        return scoreH;
    }

    public void setScoreH(int scoreH) {
        this.scoreH = scoreH;
    }

    public int getScoreG() {
        return scoreG;
    }

    public void setScoreG(int scoreG) {
        this.scoreG = scoreG;
    }

    public int getTeamH_id() {
        return teamH_id;
    }

    public void setTeamH_id(int teamH_id) {
        this.teamH_id = teamH_id;
    }

    public int getTeamG_id() {
        return teamG_id;
    }

    public void setTeamG_id(int teamG_id) {
        this.teamG_id = teamG_id;
    }
}
