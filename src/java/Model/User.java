package Model;

import java.util.UUID;

public class User {
	private String prename;
	private String surname;
	private String email;
	private String password;
        private String token;
        private boolean registered;
        private String sessionToken;
	

        public User() {            
        }

	/**
	 * Konstruktor
	 * @param prename
	 * @param surname
	 * @param email
	 * @param password
	 */
        
	public User(String prename, String surname, String email, String password) {
		this.prename = prename;
		this.surname = surname;
		this.email = email;
		this.password = password;

                this.token = UUID.randomUUID().toString();
                this.registered = false;
                this.sessionToken = null;

	}

	public String getPrename() {
		return prename;
	}
	public void setPrename(String prename) {
		this.prename = prename;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
        
        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public boolean getRegistered() {
            return registered;
        }

        public void setRegistered(boolean registered) {
            this.registered = registered;
        }
        
        public String getSessionToken() {
            return sessionToken;
        }

        public void setSessionToken(String sessionToken) {
            this.sessionToken = sessionToken;
        }
        
        @Override
        public String toString() {
            
            return new StringBuffer(" prename : ").append(this.prename).append(" surname : ").append(this.surname).append(" email : ").append(this.email).append(" password : ").append(this.password).toString();
        }
}
