package Model;

import Database.MatchDAO;
import Database.TeamDAO;
import java.util.Date;
import java.sql.Time;
import java.util.ArrayList;

public class Tournament {

    private int id;
    private Date date;
    private String name;
    private Time start_time;
    private int match_duration;
    private int pause_duration;
    private String tournament_type_type;
    private String user_email;
    private String state_type_type;
    private String visibility_type;
    
    TeamDAO teamDao = new TeamDAO();
    MatchDAO matchDao = new MatchDAO();

    public Tournament() {        
    }

    public Tournament(int id, Date date, String name, Time start_time, int match_duration, int pause_duration, String tournament_type_type, String user_email, String state_type_type, String visibility_type) {
        this.id = id;
        this.date = date;
        this.name = name;
        this.start_time = start_time;
        this.match_duration = match_duration;
        this.pause_duration = pause_duration;
        this.tournament_type_type = tournament_type_type;
        this.user_email = user_email;
        this.state_type_type = state_type_type;
        this.visibility_type = visibility_type;
        
    }
    
    public Tournament(Date date, String name, Time start_time, int match_duration, int pause_duration, String tournament_type_type, String user_email, String state_type_type, String visibility_type) {

        this.date = date;
        this.name = name;
        this.start_time = start_time;
        this.match_duration = match_duration;
        this.pause_duration = pause_duration;
        this.tournament_type_type = tournament_type_type;
        this.user_email = user_email;
        this.state_type_type = state_type_type;
        this.visibility_type = visibility_type;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Time getStart_time() {
        return start_time;
    }

    public void setStart_time(Time start_time) {
        this.start_time = start_time;
    }

    public int getMatch_duration() {
        return match_duration;
    }

    public void setMatch_duration(int match_duration) {
        this.match_duration = match_duration;
    }

    public int getPause_duration() {
        return pause_duration;
    }

    public void setPause_duration(int pause_duration) {
        this.pause_duration = pause_duration;
    }

    public String getTournament_type_type() {
        return tournament_type_type;
    }

    public void setTournament_type_type(String tournament_type_type) {
        this.tournament_type_type = tournament_type_type;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getState_type_type() {
        return state_type_type;
    }

    public void setState_type_type(String state_type_type) {
        this.state_type_type = state_type_type;
    }

    public String getVisibility_type() {
        return visibility_type;
    }

    public void setVisibility_type(String visibility_type) {
        this.visibility_type = visibility_type;
    }
        
    public void generateMatches(int tournamentId){
        
        // Variable für die Anzahl an Teams
        int numberOfTeams;
        
        // Alle Teams zu der tournament_id werden geholt und in die Liste gespeichert
        ArrayList<Team> teamList = teamDao.findAllTeamsByTournamentId(tournamentId);
        
        // Setzt anhand der Liste an Teams die Anzahl an Teams
        numberOfTeams = teamList.size(); // 3
        
        System.out.println("#generateMatches# ->" + numberOfTeams);

        // Eine leere Match Liste wird erstellt
        ArrayList<Match> matchList = new ArrayList<>();
        
        // For schleife um die Matches festzulegen und in die matchList zu speichern
        for(int i = 0; i < numberOfTeams; i++){ // 3x
            for(int x = i+1; x < numberOfTeams; x++){ //2x
                Match match = new Match(tournamentId, teamList.get(i).getId(), teamList.get(x).getId());
                matchList.add(match);
                System.out.println("Team"+(i+1) + " vs " + "Team" + (x+1) + " id ->" + id);
            }
        }
        
        // Ruft die Datenbank Funktion zum erstellen der Matches aus und übergibt dazu die Liste an Matches
        matchDao.createMatches(matchList);
        
    } 
	
}
