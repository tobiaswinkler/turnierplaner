package Model;
public class Editor {
    
    private int id;
    private String user_mail;
    private int tournamentId;

    public Editor() {
    }

    public Editor(int id, String user_mail, int tournamentId) {
        this.id = id;
        this.user_mail = user_mail;
        this.tournamentId = tournamentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_mail() {
        return user_mail;
    }

    public void setUser_mail(String user_mail) {
        this.user_mail = user_mail;
    }

    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }
  
}
