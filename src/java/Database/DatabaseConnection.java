package Database;

import java.sql.*;

public class DatabaseConnection {
    private static DatabaseConnection dbcon;
    
    DatabaseConnection(){}
    
    public static DatabaseConnection getInstance(){
        if(dbcon == null){
            dbcon = new DatabaseConnection();
        }
        return dbcon;
    }
    static Connection connection;
	
	public void openConnection(){
		String serverAddress = "195.37.176.178";
		int port = 11336;
		String db = "dbwebanw_sose16_17";
		String dbUser = "dbweb_user_17";
		String dbUserPassword = "DBqjaZ";
		try{
			Class.forName("com.mysql.jdbc.Driver");
			// Connection zum Server mit DB und Userdaten
                        
			connection = DriverManager.getConnection("jdbc:mysql://" + serverAddress + ":" + port + "/" + db, dbUser, dbUserPassword);
			//Statement wird erzeugt
			System.out.println("Database connection was successful!");
		} catch(Exception ex){
			System.out.println("Error: " + ex);
		}
	}

	
	//Getter und Setter
	public static Connection getConnection() {
		return connection;
	}

	public static void setConnection(Connection con) {
		connection = con;
	}
        
        public static boolean close(){
            try{
                connection.close();
                return true;
            }catch(Exception ex){
                System.out.println(ex);
                return false;
            }
            
        }

	
        
}

	

