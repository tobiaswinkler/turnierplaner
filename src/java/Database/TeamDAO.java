package Database;

import Model.Team;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class TeamDAO {
    final private Connection connection = DatabaseConnection.getConnection();
    private Statement statement;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public boolean createTeam(Team team, int tournamentId){
        String name = team.getName();
        
        try{
            String sql = "INSERT INTO Team (name, tournament_id) VALUES(?,?);";
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, Integer.toString(tournamentId));
            preparedStatement.executeUpdate();
            preparedStatement.close();            
            return true;
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);
            return false;
        }
    }
    
    public Team findTeamById(String searchedTournamentId){
        Team team = new Team();
        try{
            String sql = "SELECT * FROM Team WHERE id = ?;";
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, searchedTournamentId);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                 //Adds new database tuple to team object
                team.setId(Integer.parseInt(resultSet.getString("id")));
                team.setName(resultSet.getString("name"));
                team.setTournamentId(Integer.parseInt(resultSet.getString("tournament_id")));
            }
            preparedStatement.close();
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);
        }		
        return team;
    }
    
    public boolean updateTeam(int teamId, String teamName){
        try {
            String sql = "UPDATE Team SET name = ? WHERE id = ?;";
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, teamName);
            preparedStatement.setString(2, Integer.toString(teamId));
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
            return false;
        }
    }
        
    public ArrayList<Team> findAllTeamsByTournamentId(int searchedTournamentId){
        ArrayList<Team> teamList = new ArrayList<>();
        try{
            String sql = "SELECT * FROM Team WHERE tournament_id = ?;";
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, searchedTournamentId);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                String id = resultSet.getString("id");
                String name = resultSet.getString("name");
                String tournamentId = resultSet.getString("tournament_id");
                 //Adds new database tuple to team object
                Team team = new Team();  
                team.setId(Integer.parseInt(id));
                team.setName(name);
                team.setTournamentId(Integer.parseInt(tournamentId));
                //Adds user to ArrayList
                teamList.add(team);
            }
            preparedStatement.close();
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);
        }		
        return teamList;
    }
}
