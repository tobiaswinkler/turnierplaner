package Database;

import Model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserDAO{

    final private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private DatabaseConnection dbcon;
    
    public UserDAO(){
        dbcon = DatabaseConnection.getInstance();
        if(dbcon.getConnection() == null){
            dbcon.openConnection();
        }
        connection = dbcon.getConnection(); 
    }

    public boolean createUser(User user) {
        try{
            String sql = "INSERT INTO User (prename, surname, email, password, token, registered, sessionToken) "
                    + "VALUES (?, ?, ?, ?, ?, ?,?)";
            if(this.connection != null){
            System.out.println("Die: " + this.connection.toString());			

            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getPrename());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getToken());
            boolean registered = user.getRegistered();
        String strRegistered;
        if(registered == false){
            strRegistered = "0";
        }else{
            strRegistered = "1"; 
        }
            preparedStatement.setString(6, strRegistered);
            preparedStatement.setString(7, user.getSessionToken());
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
           }
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);
            return false;
        }
        return false;
    }
    
    // Setzt den Token des Users auf 1
    public int registerToken(String token) {
        try{
            String sql = "UPDATE User SET registered = ? WHERE token=?";

            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, 1);
            preparedStatement.setString(2, token);
            
            int rowsUpdated = preparedStatement.executeUpdate();
            System.out.println("RowsUpdated: " + rowsUpdated);
            preparedStatement.close();
            return rowsUpdated;
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);	
            return 0;
        }
    }

    public boolean updateUser(User user) {
        try{
            String sql = "UPDATE User SET prename = ?, surname = ?, email = ?, password = ? WHERE user_id=?";

            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, user.getPrename());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            
            int rowsUpdated = preparedStatement.executeUpdate();
            System.out.println("RowsUpdated: " + rowsUpdated);
            preparedStatement.close();
            return true;
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);	
            return false;
        }
    }
    
    public ArrayList<String> findAllUnregistered() {
        ArrayList<String> tokenList = new ArrayList<>();
        try{
            String sql = "SELECT token FROM User WHERE registered = ?";
            preparedStatement = this.connection.prepareStatement(sql);            
            preparedStatement.setInt(1, 0);
            
            resultSet = preparedStatement.executeQuery();
            System.out.println("Records from Database:");
            while(resultSet.next()){
                
                String token = resultSet.getString("token");
           
                System.out.println(token);
                
                //Adds user to ArrayList
                tokenList.add(token);
            }
            preparedStatement.close();
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);
        }		
        return tokenList;
    }

    public ArrayList<User> findAllUsers() {
        ArrayList<User> userList = new ArrayList<>();
        try{
            String sql = "SELECT * FROM User";
            statement = (Statement) this.connection.createStatement();
            resultSet = statement.executeQuery(sql);
            System.out.println("Records from Database:");
            while(resultSet.next()){
                //Adds new database tuple to user object
                User user = new User();
                user.setPrename(resultSet.getString("prename"));
                user.setSurname(resultSet.getString("surname"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setToken(resultSet.getString("token"));
                String strRegistered = resultSet.getString("registered");
                boolean registered;
                if(strRegistered.equals("0")){
                    registered = false;
                }else{
                    registered = true;
                }
                user.setRegistered(registered);
                user.setSessionToken(resultSet.getString("sessionToken"));
                //Adds user to ArrayList
                userList.add(user);
            }
            statement.close();
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);
        }		
        return userList;
    }

    public boolean isRegistered(String searchedEmail) {
        
        boolean isRegistered = false;
        
        try{
            //SQL-Abfrage
            String sql = "SELECT registered FROM User WHERE email = ?;";
            //PreparedStatement wird erstellt
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, searchedEmail);
            //Abfrage wird ausgeführt und in den ResultSet rs geschrieben
            resultSet = preparedStatement.executeQuery();
            //für jeden Eintage im ResultSet werden folgende Befehle ausgeführt
            //hier nur ein ResultSet
            while(resultSet.next()){
   
                String strRegistered = resultSet.getString("registered");
          
                if(strRegistered.equals("0")){
                    isRegistered = false;
                }else{
                    isRegistered = true;
                }

            }
            preparedStatement.close();
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);
        }
        return isRegistered;       
    }
    

    public void deleteUser(User user) {
            // TODO Auto-generated method stub
    }

    public User findUserByEmail(String searchedEmail) {
        User user = new User();
        try{
            //SQL-Abfrage
            String sql = "SELECT * FROM User WHERE email = ?;";
            //PreparedStatement wird erstellt
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, searchedEmail);
            //Abfrage wird ausgeführt und in den ResultSet rs geschrieben
            resultSet = preparedStatement.executeQuery();
            //für jeden Eintage im ResultSet werden folgende Befehle ausgeführt
            //hier nur ein ResultSet
            while(resultSet.next()){
                user.setPrename(resultSet.getString("prename"));
                user.setSurname(resultSet.getString("surname"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setToken(resultSet.getString("token"));
                String strRegistered = resultSet.getString("registered");
                boolean registered;
                if(strRegistered.equals("0")){
                    registered = false;
                }else{
                    registered = true;
                }
                user.setRegistered(registered);
                user.setSessionToken(resultSet.getString("sessionToken"));
            }
            preparedStatement.close();
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);
        }
        return user;       
    }
    
    public boolean updateSessionToken(String email, String sessionToken){
        try{
            String sql = "UPDATE User SET sessionToken = ? WHERE email=?";

            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, sessionToken);
            preparedStatement.setString(2, email);
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);	
            return false;    
        }
    }
}
