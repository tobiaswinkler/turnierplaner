package Database;

import Model.Match;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class MatchDAO {
    final private Connection connection = DatabaseConnection.getConnection();
    private Statement statement;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    
    public boolean createMatches(ArrayList<Match> matchList){
        boolean success = false;
        
        for(Match match : matchList){
            try{
                String sql = "INSERT INTO `Match` (scoreH, scoreG, tournament_id, teamH_id, teamG_id) "
                    + "VALUES(?, ?, ?, ?, ?);";

                if(this.connection != null){
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setString(1, String.valueOf(match.getScoreH()));
                preparedStatement.setString(2, String.valueOf(match.getScoreG()));
                preparedStatement.setString(3, String.valueOf(match.getTournament_id()));
                preparedStatement.setString(4, String.valueOf(match.getTeamH_id()));
                preparedStatement.setString(5, String.valueOf(match.getTeamG_id())); 
                preparedStatement.executeUpdate();
                success = true;
               }
            }catch(SQLException ex){
                System.out.println("Query-Error: " + ex);
            }     
        }
        
        return success;
    }
    
    public Match findMatchById(int matchId){
        Match match = new Match();
        
        try {
            String sql = "SELECT * FROM `Match` WHERE id = ?;";
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, Integer.toString(matchId));
            resultSet = preparedStatement.executeQuery();
            match.setId(resultSet.getInt("m.id"));
            match.setScoreH(resultSet.getInt("m.scoreH"));
            match.setScoreG(resultSet.getInt("m.scoreG"));
            match.setTournament_id(resultSet.getInt("m.tournament_id"));
            match.setTeamH_id(resultSet.getInt("m.teamH_id"));
            match.setTeamG_id(resultSet.getInt("m.teamG_id"));        
            preparedStatement.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
        return match;
    }
    
    public ArrayList<Match> getAllMatches(int tournamentId){
        ArrayList<Match> matchList = new ArrayList<>();
        try {
            String sql = "SELECT m.id, m.scoreH, m.scoreG, m.tournament_id, m.teamH_id, m.teamG_id "
                    + "FROM `Match` AS m, `Tournament` AS t "
                    + "WHERE t.id = m.tournament_id AND t.id = ?;";
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, Integer.toString(tournamentId));
            resultSet = preparedStatement.executeQuery();
            Match match = new Match();
            match.setId(resultSet.getInt("m.id"));
            match.setScoreH(resultSet.getInt("m.scoreH"));
            match.setScoreG(resultSet.getInt("m.scoreG"));
            match.setTournament_id(resultSet.getInt("m.tournament_id"));
            match.setTeamH_id(resultSet.getInt("m.teamH_id"));
            match.setTeamG_id(resultSet.getInt("m.teamG_id"));        
            preparedStatement.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
        return matchList;
    }
    
    public boolean updateResult(int matchId, int scoreH, int scoreG){
        try {
            String sql = "UPDATE `Match` SET scoreH = ?, scoreG = ? WHERE id = ? ";
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, Integer.toString(scoreH));
            preparedStatement.setString(2, Integer.toString(scoreG));
            preparedStatement.setString(3, Integer.toString(matchId));
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
            return false;
        }
    }
}