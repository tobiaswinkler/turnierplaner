package Database;

import Model.Tournament;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;

public class TournamentDAO {
    final private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private DatabaseConnection dbcon;
    
    public TournamentDAO(){
     dbcon = DatabaseConnection.getInstance();
        if(dbcon.getConnection() == null){
            dbcon.openConnection();
        }
        connection = dbcon.getConnection();
    }
    
    //Creates tournament in database
    public int createTournament(Tournament tournament) {        
        int trnmtnId = 0;
        //Id for the last inserted id
        int id = 0;
        //Attributes for the tournament
        Date date = new Date(tournament.getDate().getTime());
        Time start_time = tournament.getStart_time();
        int match_duration = tournament.getMatch_duration();
        int pause_duration = tournament.getPause_duration();
        
        try{
            //MySQL command for interting values
            String sql = "INSERT INTO Tournament (date, name, start_time, match_duration, pause_duration, tournament_type_type, user_email, state_type_type, visibility_type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
            //Builds preparedStatement
            preparedStatement = this.connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setDate(1, date);
            preparedStatement.setString(2, tournament.getName());
            preparedStatement.setTime(3, start_time);
            preparedStatement.setInt(4, match_duration);
            preparedStatement.setInt(5, pause_duration);

            preparedStatement.setString(6, tournament.getTournament_type_type());
            preparedStatement.setString(7, tournament.getUser_email());
            preparedStatement.setString(8, tournament.getState_type_type());
            preparedStatement.setString(9, tournament.getVisibility_type());

            System.out.println("Statement: " + preparedStatement.toString());

            preparedStatement.executeUpdate();
             ResultSet rs = preparedStatement.getGeneratedKeys();

             trnmtnId = 1;
             while(rs.next()){
                 System.out.println(rs.getInt(trnmtnId));
                 trnmtnId = rs.getInt(trnmtnId);
             }
            //id = getLastInsertedId();
            preparedStatement.close();
            return trnmtnId;
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);
            return trnmtnId;
        }
    }
    
    //Updates a tournament
    public boolean updateTournament(Tournament tournament) {
        try{
            //MySQL command for interting values
            String sql = "UPDATE Tournament SET name = ?, start_time = ?, match_duration = ?, pause_duration = ?, tournament_type_type = ?, user_email = ?, state_type_type = ?, visibility_type = ? WHERE id = ?;";
            //Builds preparedStatement
            preparedStatement = this.connection.prepareStatement(sql);
            //preparedStatement.setDate(1, (java.sql.Date)tournament.getDate());
            preparedStatement.setString(1, tournament.getName());
            preparedStatement.setTime(2, tournament.getStart_time());
            preparedStatement.setInt(3, tournament.getMatch_duration());
            preparedStatement.setInt(4, tournament.getPause_duration());
            preparedStatement.setString(5, tournament.getTournament_type_type());
            preparedStatement.setString(6, tournament.getUser_email());
            preparedStatement.setString(7, tournament.getState_type_type());
            preparedStatement.setString(8, tournament.getVisibility_type());
            preparedStatement.setString(9, Integer.toString(tournament.getId()));
            //Executes preparedStatement
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);
            return false;
        }
    }
    
    //Selects all torunaments
    public ArrayList<Tournament> findAllTournaments() {
        //Declares an ArrayList with the of Tournament for adding tournaments
        ArrayList<Tournament> tournamentList = new ArrayList<>();
        try{
            //MySQL command for selecting all tournament
            String sql = "SELECT * FROM Tournament";
            //Builds statement
            statement = (Statement) this.connection.createStatement();
            //Writes results in resultSet
            resultSet = statement.executeQuery(sql);
            System.out.println("Records from Database:");
            //Loops resultSet
            while(resultSet.next()){  
                //Declares tournament object for initialising from database
                Tournament tournament = new Tournament();
                tournament.setId(resultSet.getInt("id"));
                tournament.setName(resultSet.getString("name"));
                tournament.setDate(resultSet.getDate("date"));
                tournament.setStart_time(resultSet.getTime("start_time"));
                tournament.setMatch_duration(resultSet.getInt("match_duration"));
                tournament.setPause_duration(resultSet.getInt("pause_duration"));
                tournament.setTournament_type_type(resultSet.getString("tournament_type_type"));
                tournament.setUser_email(resultSet.getString("user_email"));
                tournament.setState_type_type(resultSet.getString("state_type_type"));
                tournament.setVisibility_type(resultSet.getString("visibility_type"));
                //Adds tournaments to tournamentList
                tournamentList.add(tournament);
            }
            preparedStatement.close();
        }catch(SQLException ex){
            System.out.println("Query-Error: " + ex);
        }		
        return tournamentList;
    }
    
    //Deletes tournament
    public boolean deleteTournament(int tournamentId) {
        try {
            //MySQL command for deleting a certain tournament
            String sql = "DELETE FROM Tournament WHERE id = ?";
            //Builds preparedStatement
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, Integer.toString(tournamentId));
            preparedStatement.executeUpdate();
            preparedStatement.close();
            return true;
        } catch (SQLException ex) {
            System.out.println("Error " + ex);
            return false;
        }        
    }

    //Finds tournament by tournament
    public Tournament findTournamentById(int tournamentId) {
        Tournament tournament = new Tournament();
        try{
            //MySQL command for selection a certain tournament
            String sql = "SELECT * FROM Tournament WHERE id = ?;";
            //Build preparedStatement
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, Integer.toString(tournamentId));
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){                
                tournament.setId(resultSet.getInt("id"));
                tournament.setName(resultSet.getString("name"));
                tournament.setDate(resultSet.getDate("date"));
                //tournament.setStart_time(resultSet.getTime("start_time"));
                tournament.setMatch_duration(resultSet.getInt("match_duration"));
                tournament.setPause_duration(resultSet.getInt("pause_duration"));
                tournament.setTournament_type_type(resultSet.getString("tournament_type_type"));
                tournament.setUser_email(resultSet.getString("user_email"));
                tournament.setState_type_type(resultSet.getString("state_type_type"));
                tournament.setVisibility_type(resultSet.getString("visibility_type"));
            }
            preparedStatement.close();
        }catch(SQLException ex){
            System.out.println("Error: " + ex);
        }
        return tournament;
    }

    public ArrayList<Tournament> findTournamentsByOwner(String email) {
        ArrayList<Tournament> tournamentList = new ArrayList<>();
        try{
            String sql = "SELECT * FROM Tournament WHERE user_email = ?;";
            
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();
            System.out.println("Records from Database:");
            while(resultSet.next()){               
          
                Tournament tournament = new Tournament();
                tournament.setId(resultSet.getInt("id"));
                tournament.setName(resultSet.getString("name"));
                tournament.setDate(resultSet.getDate("date"));
                //tournament.setStart_time(resultSet.getTime("start_time"));
                tournament.setMatch_duration(resultSet.getInt("match_duration"));
                tournament.setPause_duration(resultSet.getInt("pause_duration"));
                tournament.setTournament_type_type(resultSet.getString("tournament_type_type"));
                tournament.setUser_email(resultSet.getString("user_email"));
                tournament.setState_type_type(resultSet.getString("state_type_type"));
                tournament.setVisibility_type(resultSet.getString("visibility_type"));
               
                tournamentList.add(tournament);
            }
            
            preparedStatement.close();
        }catch(SQLException ex){
            System.out.println("Error: " + ex);
        }
        return tournamentList;
    }
    
    public ArrayList<Tournament> findTournamentsByEditor(String email){
        ArrayList<Tournament> tournamentList = new ArrayList<>();
        try{
            String sql = "SELECT t.id, t.date, t.name, t.start_time, t.match_duration,"
                    + " t.pause_duration, t.tournament_type_type, t.user_email, t.state_type_type, "
                    + "t.visibility_type "
                    + "FROM Tournament AS t, Editor AS e, User u "
                    + "WHERE t.id = e.tournament_id AND u.email = e.user_email AND "
                    + "u.email = ?;";
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();
            System.out.println("Records from Database:");
            while(resultSet.next()){                
                Tournament tournament = new Tournament();
                tournament.setId(resultSet.getInt("id"));
                tournament.setName(resultSet.getString("name"));
                tournament.setDate(resultSet.getDate("date"));
                //tournament.setStart_time(resultSet.getTime("start_time"));
                tournament.setMatch_duration(resultSet.getInt("match_duration"));
                tournament.setPause_duration(resultSet.getInt("pause_duration"));
                tournament.setTournament_type_type(resultSet.getString("tournament_type_type"));
                tournament.setUser_email(resultSet.getString("user_email"));
                tournament.setState_type_type(resultSet.getString("state_type_type"));
                tournament.setVisibility_type(resultSet.getString("visibility_type"));
                tournamentList.add(tournament);
            }
            preparedStatement.close();
        }catch(SQLException ex){
            System.out.println("Error: " + ex);
        }
        return tournamentList;
    }
}
