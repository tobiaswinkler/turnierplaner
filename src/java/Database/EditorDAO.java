package Database;

import Model.Editor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class EditorDAO {
    final private Connection connection = DatabaseConnection.getConnection();
    private Statement statement;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private DatabaseConnection dbcon;
    
    public boolean createEditor(String email, int tournamentId){
        int id = 0;
        try {
            String sql = "INSERT INTO Editor (user_email, tournament_id) VALUES (?,?);";
            preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, Integer.toString(tournamentId));
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            id = 1;
            while(rs.next()){
                System.out.println(rs.getInt(id));
                id = rs.getInt(id);
            }
            preparedStatement.close();
            return true;
        }catch (SQLException ex) {
            System.out.println("Error: " + ex);
            return false;
        }
    }
    
    public Editor findEditorbyId(int editorId){
        Editor editor = new Editor();
        try {
            String sql = "SELECT * FROM Editor WHERE id = ?;";
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, Integer.toString(editorId));
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                editor.setId(Integer.parseInt("id"));
                editor.setUser_mail("user_email");
                editor.setTournamentId(Integer.parseInt("tournament_id"));
            }
            preparedStatement.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
        return editor;
    }
    
    public ArrayList<Editor> findEditorsByTournament(int tournamentId){
        ArrayList<Editor> editorList = new ArrayList<>();
        try {
            String sql = "SELECT * FROM Editor WHERE tournament_id = ?;";
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, Integer.toString(tournamentId));
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                Editor editor = new Editor();
                editor.setId(Integer.parseInt("id"));
                editor.setUser_mail("user_email");
                editor.setTournamentId(Integer.parseInt("tournament_id"));
                editorList.add(editor);
            }
            preparedStatement.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
        return editorList;
    }
    
    public ArrayList<Editor> findEditorsByEmail(String email){
        ArrayList<Editor> editorList = new ArrayList<>();
        try {
            String sql = "SELECT * FROM Editor WHERE user_email = ?;";
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                Editor editor = new Editor();
                editor.setId(Integer.parseInt("id"));
                editor.setUser_mail("user_email");
                editor.setTournamentId(Integer.parseInt("tournament_id"));
                editorList.add(editor);
            }
            preparedStatement.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex);
        }
        return editorList;
    }
}
