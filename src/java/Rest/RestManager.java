package Rest;

import javax.ws.rs.core.NewCookie;
public class RestManager {
    
    public RestManager(){
    }
    
     public NewCookie createCookie(String cookieName, String cookieValue) {
        String path = "/";
        int maxAge = -1;
        boolean httpOnly = true;
        boolean secure = true;
        String domain = "";
        String comment = "";
        
        NewCookie cookie = new NewCookie(cookieName, cookieValue, path, domain, comment, maxAge, secure, httpOnly);

        return cookie;
    }
    
}

