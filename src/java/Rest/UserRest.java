package Rest;

import Model.User;
import Controller.UserManager;
import Model.Tournament;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Date;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.*;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.NewCookie;

@Path("user")
public class UserRest{
    UserManager userMngr = new UserManager();
    RestManager restMngr = new RestManager();
    Gson gson = new Gson();
    RespondMessages respondm = new RespondMessages();
    
    @GET
    @Path("getuser")
    public String getuser()
    {
        User user = new User("restUser", "restUser1", "rest@web.de", "rest1234");
        JSONArray userArray = new JSONArray();
        JSONObject jsnUser = new JSONObject();
        jsnUser.put("prename", user.getPrename());
        jsnUser.put("surname", user.getSurname());
        jsnUser.put("email", user.getEmail());
        jsnUser.put("password", user.getPassword()); 
        userArray.put(jsnUser);
        return userArray.toString();
    }
   
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("createUser")
    public Response createUser(String user){
        
        //String prename, String surname, String email, String password 
        System.out.println("REST user: " + user);
        JSONObject userArr = new JSONObject(user);
 
         String prename = userArr.getString("prename");           
         String surname = userArr.getString("surname");
         String email = userArr.getString("email");
         String password = userArr.getString("password");
         
         User newUser = new User(prename, surname, email, password);
         
      if(!userMngr.checkEmailExists(email)){
        if(userMngr.createUser(newUser)){
           return Response.status(200).entity(respondm.RegisterSuccessfull()).build();
        }else{
           return Response.status(204).entity(respondm.Error()).build();
        }
      }else {
          return Response.status(204).entity(respondm.UsernameExists()).build();
      }
         
    }
    
    // Soll den übergebenen token in der Db suchen und diesen auf 1 setzten
    @GET @Produces( MediaType.TEXT_PLAIN )
    @Path("token/{token}")
    public String tokenRegister( @PathParam("token") String token )
    {
        
        int res = userMngr.registerToken(token);
        
        if(res == 0){
            return String.format( "registerToken = false");
        } else {
            return String.format( "registerToken = true");
        }   
    }

    @GET 
    @Produces(MediaType.TEXT_PLAIN)
    @Path("checkuserbyemail/{email}")
    public Response checkUserByEmail( @PathParam("email") String email )
    {
       boolean exists = userMngr.checkEmailExists(email);
       
       if(exists){
           return Response.ok().build();
       }
       else{
           return Response.status(204).build();
       }
    }

    @GET
    @Path("token")
    public String showUnregistered()
    {
        ArrayList<String> tokenList = userMngr.findAllUnregistered();
        
        String output = "";
        
        for(int i = 0; i < tokenList.size(); i++){
            output = output + "<a href='/TurnierPlanerWebApp/rest/user/token/"+ tokenList.get(i) +"'>"+tokenList.get(i)+"</a><br>";
        }
        
        return output;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("login")
    public Response loginuser(String userLogin){
        System.out.println("LOGINREST");
         //User und PW in der DB abfragen
        JSONObject userArr = new JSONObject(userLogin);
        
        String email = userArr.getString("email");
        String password = userArr.getString("password");
         
        if(userMngr.checkUserLogin(email, password)){
            if(userMngr.isRegistered(email)){
                
                Date timestamp = new Date();

                NewCookie cookie = restMngr.createCookie(email, timestamp.toString());

                //Session Token in die Datenbank schreiben
                if(userMngr.setCookie(email, timestamp.toString())){
                     User user = userMngr.getUserByEmail(email);
                     String userJsn = gson.toJson(user); 
                    //Antwort OK zurückgeben
                    return Response.status(200).entity(userJsn).cookie(cookie).build();

                }
                //Fehler zurückgeben
                return Response.status(204).entity("").cookie().build();
            }
            //Fehler zurückgebe
            return Response.status(203).entity("").cookie().build();
        }
        //Fehler zurückgeben
        System.out.println(respondm.LoginError());
        return Response.status(204).entity(respondm.LoginError()).build();
    }
}
