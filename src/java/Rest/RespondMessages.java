package Rest;

import com.google.gson.Gson;
import org.json.JSONObject;
public class RespondMessages {
    
    private String loginSuccessfull;
    private String loginError;
    private String usernameExists;
    private String tournamentCreated;
    private String tournamentError;
    private String registerSuccessfull;
    private String error;
    private String timeout;
    private String deleteSuccess;
    private String deleteError;

    
    private Gson gson;       
      
    public RespondMessages(){ 
        gson = new Gson();
        tournamentCreated = "Turnier wurde erfolgreich angelegt";
        tournamentError = "Fehler beim anlegen des Turniers";
        loginSuccessfull = "Login erfolgreich";
        loginError = "E-Mail-Adresse oder Passwort falsch";
        usernameExists = "Ein Benutzer mit dieser E-Mail-Adresse ist schon vorhanden";
        registerSuccessfull = "Registrierung erfolgreich";
        error = "Fehler";
        timeout = "Anmeldung erforderlich";
        deleteSuccess = "Erfolgreich gelöscht";
        deleteError = "Fehler beim löschen";
    }
    
    public Object TournamentCreated(){
        return gson.toJson(tournamentCreated);
    }
    public Object TournamentError(){
        return gson.toJson(tournamentError);
    }
    
    public Object LoginSuccessfull(){
        return gson.toJson(loginSuccessfull);
    }
    public Object LoginError(){
        return gson.toJson(loginError);
    }
    public Object UsernameExists(){
        return gson.toJson(usernameExists);
    }
   
    public Object RegisterSuccessfull(){
        return gson.toJson(registerSuccessfull);
    }
    
    public Object Error(){
        return gson.toJson(error);
    }
    public Object Timeout(){
        return gson.toJson(timeout);
    }

    public Object DeleteSuccessful() {
      return gson.toJson(deleteSuccess);
    }

    public Object DeleteError() {
      return gson.toJson(deleteError);
    }
    
}
