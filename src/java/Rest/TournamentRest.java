package Rest;

import Controller.TeamManager;
import Controller.TournamentManager;
import Model.Team;
import Model.Tournament;
import com.google.gson.Gson;
import static java.awt.event.PaintEvent.UPDATE;
import java.util.Date;
import java.util.ArrayList;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONObject;

@Path("/tournaments")
public class TournamentRest {

    TournamentManager tournamentmngr = new TournamentManager();
    TeamManager teammngr = new TeamManager();
    RespondMessages respondm = new RespondMessages();
    Gson gson = new Gson();

    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("gettournamentbyId/{id}")
    public Response getTournamentById(@PathParam("id") String id) {
        int tid = Integer.parseInt(id);
        Tournament tournament = tournamentmngr.findTournamentById(tid);
        JSONObject jsntrnmnt = new JSONObject(tournament);
        ArrayList<Team> teamsArray = teammngr.getTeamsByTournamentId(tournament.getId());
        jsntrnmnt.put("teams", teamsArray);

        String result = jsntrnmnt.toString();
        return Response.ok(result).build();
    }

    @DELETE
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("deleteTournament/{id}")
    public Response deleteTournament(@PathParam("id") String id) {
        boolean result = tournamentmngr.DeleteTournamentById(id);
        if (result) {
            return Response.ok(respondm.DeleteSuccessful().toString()).build();

        } else {
            return Response.status(204).entity(respondm.DeleteError()).build();
        }
    }

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("addEditor/{user}/{index}")
    public Response addEditor(@PathParam("user") String user, @PathParam("index") String index) {
        boolean result = tournamentmngr.addEditor(user, index);
        if (result) {
            return Response.status(200).entity("").build();
        } else {
            return Response.status(204).entity("").build();
        }

    }

    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("geteditabletournament/{email}")
    public Response getTournamentsAsEditor(@PathParam("email") String email) {
        ArrayList<Tournament> tournaments = new ArrayList<>();
        tournaments = tournamentmngr.findTournamentAsEditor(email);
        JSONArray mJSONArray = new JSONArray();
        for (Tournament trnmnt : tournaments) {
            JSONObject tournament = new JSONObject();
            tournament.put("tournamentId", trnmnt.getId());
            tournament.put("name", trnmnt.getName());
            tournament.put("match_duration", trnmnt.getMatch_duration());
            tournament.put("tournament_type_type", trnmnt.getTournament_type_type());
            //tournament.put("count", t.get());

            JSONArray teams = new JSONArray();
            ArrayList<Team> teamsArray = teammngr.getTeamsByTournamentId(trnmnt.getId());
            //Wenn das Turnier nicht angelegt werden konnte, Fehler zurückgeben
            if (trnmnt.getId() == 0) {
                return Response.status(200).entity(respondm.TournamentError()).build();
            }
            for (Team tm : teamsArray) {
                JSONObject team = new JSONObject();
                team.put("id", tm.getId());
                team.put("name", tm.getName());
                teams.put(team);
            }

            tournament.put("count", teamsArray.size());
            tournament.put("teams", teams);
            tournament.put("state", trnmnt.getState_type_type());
            mJSONArray.put(tournament);
        }
        JSONObject allTournaments = new JSONObject();
        if (tournaments.size() > 0) {
            allTournaments.put("tournaments", mJSONArray);
        } else {
            allTournaments.put("tournaments", "leer");
        }
        String result = allTournaments.toString();

        return Response.ok(result).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("createtournament")
    public Response createTournament(String trnmt) {
        //Daten des Turniers in ein Turnier Objekt speichern
        JSONObject trnmtArr = new JSONObject(trnmt);

        String name = trnmtArr.getString("name");
        int duration = trnmtArr.getInt("match_duration");
        String type = trnmtArr.getString("tournament_type_type");
        String status = trnmtArr.getString("state_type_type");
        String email = trnmtArr.getString("email");

        Tournament tournament = new Tournament();

        tournament.setName(name);
        tournament.setMatch_duration(duration);
        tournament.setTournament_type_type(type);
        tournament.setState_type_type(status);
        tournament.setUser_email(email);
        Date date = new Date();
        tournament.setDate(date);
        tournament.setStart_time(java.sql.Time.valueOf("00:00:00"));
        tournament.setVisibility_type("offen");
        
        int id = 0;
        boolean update = false;
        //Turnier Objekt in die DB speichern
        if (trnmtArr.getInt("id") > 0) {
            id = trnmtArr.getInt("id");
            tournament.setId(id);
            tournamentmngr.update(tournament);
            update = true;
        } else {
            id = tournamentmngr.createTournament(tournament);
        }
        if (id != 0) {
            //Namen der Teams auslesen und auch in die DB speichern
            JSONArray teamsArr = trnmtArr.getJSONArray("teams");
            ArrayList<Team> teams = new ArrayList<>();

            for (int i = 0; i < teamsArr.length(); i++) {
                JSONObject teamJsn = teamsArr.getJSONObject(i);
                Team team = new Team();
                team.setName(teamJsn.getString("name"));
                teams.add(team);
            }

            for (Team t : teams) {
                if (update) {
                    if (teammngr.updateTeam(t, id) == false) {
                        //Wenn das anlegen des Teams in der Datenbank nicht funktioniert, 
                        //Fehler ausgeben
                        return Response.status(204).entity(respondm.TournamentError()).build();
                    }
                } else if (teammngr.createTeam(t, id) == false) {
                    //Wenn das anlegen des Teams in der Datenbank nicht funktioniert, 
                    //Fehler ausgeben
                    return Response.status(204).entity(respondm.TournamentError()).build();
                }
            }
            //Anlegen des Turniers erfolgreich
            return Response.status(200).entity(respondm.TournamentCreated()).build();
        }
        return Response.status(204).entity(respondm.TournamentCreated()).build();

    }
    
 
    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("gettournaments/{email}")
    public Response getTournaments(@PathParam("email") String email) {
        ArrayList<Tournament> tournaments = new ArrayList<>();
        tournaments = tournamentmngr.findTournamentsByOwner(email);
        JSONArray mJSONArray = new JSONArray();
        for (Tournament trnmnt : tournaments) {
            JSONObject tournament = new JSONObject();
            tournament.put("tournamentId", trnmnt.getId());
            tournament.put("name", trnmnt.getName());
            tournament.put("duration", trnmnt.getMatch_duration());
            tournament.put("type", trnmnt.getTournament_type_type());
            //tournament.put("count", t.get());

            JSONArray teams = new JSONArray();
            ArrayList<Team> teamsArray = teammngr.getTeamsByTournamentId(trnmnt.getId());
            //Wenn das Turnier nicht angelegt werden konnte, Fehler zurückgeben
            if (trnmnt.getId() == 0) {
                return Response.status(200).entity(respondm.TournamentError()).build();
            }
            for (Team tm : teamsArray) {
                JSONObject team = new JSONObject();
                team.put("id", tm.getId());
                team.put("name", tm.getName());
                teams.put(team);
            }

            tournament.put("count", teamsArray.size());
            tournament.put("teams", teams);
            tournament.put("state", trnmnt.getState_type_type());
            mJSONArray.put(tournament);
        }
        JSONObject allTournaments = new JSONObject();
        allTournaments.put("tournaments", mJSONArray);
        String result = allTournaments.toString();
        return Response.ok(result).build();
    }

}
