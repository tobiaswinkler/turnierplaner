package Controller;

import Database.TeamDAO;
import Model.Team;
import java.util.ArrayList;

public class TeamManager {
    TeamDAO teamDao;
    
    
    public TeamManager(){
        teamDao = new TeamDAO();
    }
    
    public boolean createTeam(Team team, int id){
        return teamDao.createTeam(team, id);
    }
    
    public ArrayList<Team> getTeamsByTournamentId(int id){
        String sid = new Integer(id).toString();
        return teamDao.findAllTeamsByTournamentId(sid);
    }

    public boolean updateTeam(Team t, int id) {
     return teamDao.updateTeam(id, t.getName());
   }

    
}

