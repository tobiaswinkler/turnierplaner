package Controller;

import Database.UserDAO;
import Model.User;
import java.util.ArrayList;

public class UserManager {
    
    private UserDAO userDao = new UserDAO();
    private User user;
    
    public UserManager(){        
    }
    
    public User getUserByEmail(String email){
        //Wenn aktueller Nutzer Objekt schon vorhanden ist, zurückgeben
        if(user != null)
        { 
            if(user.getEmail().equals(email)){
                return user;
            }
        }
        //Ansonsten erneut aus der Datenbank nehmen
        return userDao.findUserByEmail(email);
 
    }
    
    public int registerToken(String token){        
        return userDao.registerToken(token);
        //return -1;
    }
    
    public boolean createUser(User user){
        //Nutzer in der Datenbank Anlegen
        return userDao.createUser(user);
    }
    
    public boolean isRegistered(String email){
        return userDao.isRegistered(email);
    }
    
    public ArrayList<String> findAllUnregistered(){
        return userDao.findAllUnregistered();
    }
    
    public boolean checkUserLogin(String email, String password){
        user = userDao.findUserByEmail(email);
        //Prüfen, ob das eingegebene Password mit dem in der Datenbank überstimmt
       if(user.getEmail() == null){
           return false;
       }
       if(!user.getPassword().equals(password)){
            return false;
       }
       //Prüfen ob User verifiziert/registriert wurde
       if(!user.getRegistered()){
          return false;
       }
        return true ;
    }
    public boolean setCookie(String email, String timestamp){
        return userDao.updateSessionToken(email, timestamp);
    }
    public boolean checkEmailExists(String email) {
        User user = userDao.findUserByEmail(email);
        if(user.getEmail() != null && email != null){
            if (user.getEmail().equals(email)) {
                return true;
            }
        }
        
        return false;
    }

}
