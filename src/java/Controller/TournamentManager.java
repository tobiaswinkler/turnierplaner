package Controller;

import Database.EditorDAO;
import Database.TournamentDAO;
import Model.Tournament;
import java.util.ArrayList;

public class TournamentManager {
    private TournamentDAO tournamentDao;
    private EditorDAO editorDao;
    
    public TournamentManager(){
        tournamentDao = new TournamentDAO();
        editorDao = new EditorDAO();
    }
    
    public int createTournament(Tournament tournament){
       return tournamentDao.createTournament(tournament);
    }
    
   public ArrayList<Tournament> getTournaments(){
       return  tournamentDao.findAllTournaments();
   }

    public ArrayList<Tournament> findTournamentsByOwner(String email) {
        return tournamentDao.findTournamentsByOwner(email);
    }
    
    public ArrayList<Tournament> findTournamentAsEditor(String email){
            return tournamentDao.findTournamentsByEditor(email);
    }
      
    public Tournament findTournamentById(int id){
        return tournamentDao.findTournamentById(id);
    }
    
    public boolean DeleteTournamentById(String id) {
        return tournamentDao.deleteTournament(Integer.parseInt(id));
    
    }

    public boolean update(Tournament tournament) {
       return tournamentDao.updateTournament(tournament);
    }

    public boolean addEditor(String user, String index) {
        int id = Integer.parseInt(index);
        return editorDao.createEditor(user,id);
    }
}